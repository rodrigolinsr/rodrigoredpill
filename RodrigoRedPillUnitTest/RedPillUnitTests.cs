﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RodrigoRedPillUnitTest.RedPill;
using System.ServiceModel;

namespace RodrigoRedPillUnitTest
{
    [TestClass]
    public class RedPillUnitTests
    {
        RedPillClient client = new RedPillClient();

        [TestMethod]
        public void FibonacciNumberTests()
        {
            long expected, actual;

            expected = 144L;
            actual = this.client.FibonacciNumber(12L);
            Assert.AreEqual(expected, actual);

            expected = -121393L;
            actual = this.client.FibonacciNumber(-26L);
            Assert.AreEqual(expected, actual);

            expected = 0L;
            actual = this.client.FibonacciNumber(0L);
            Assert.AreEqual(expected, actual);

            expected = -1;
            actual = this.client.FibonacciNumber(-2L);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        // [ExpectedException(typeof(FaultException))] // Didn't find a way to put this to work
        // The expected is "FaultException" but it receives a "FaultException`1" exception
        public void FibonacciNumberTestException()
        {
            try
            {
                this.client.FibonacciNumber(100L);
            }
            catch (FaultException ex)
            {
                Assert.IsTrue(ex.Message.Contains("will cause a 64-bit integer overflow"));
            }
        }

        [TestMethod]
        public void ReverseWordsTests()
        {
            string expected, actual;

            expected = "ogirdoR sniL seugirdoR";
            actual = this.client.ReverseWords("Rodrigo Lins Rodrigues");
            Assert.AreEqual(expected, actual);

            expected = "malayalam rotator";
            actual = this.client.ReverseWords("malayalam\nrotator");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        // [ExpectedException(typeof(FaultException))] // Didn't find a way to put this to work
        // The expected is "FaultException" but it receives a "FaultException`1" exception
        public void ReverseWordsTestException()
        {
            try
            {
                this.client.ReverseWords(null);
            }
            catch (FaultException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Value cannot be null"));
            }
        }

        [TestMethod]
        public void WhatShapeIsThisTests()
        {
            TriangleType expected, actual;

            expected = TriangleType.Equilateral;
            actual = this.client.WhatShapeIsThis(4, 4, 4);
            Assert.AreEqual(expected, actual);

            expected = TriangleType.Isosceles;
            actual = this.client.WhatShapeIsThis(5, 4, 4);
            Assert.AreEqual(expected, actual);

            expected = TriangleType.Scalene;
            actual = this.client.WhatShapeIsThis(5, 4, 3);
            Assert.AreEqual(expected, actual);

            // Errors
            expected = TriangleType.Error;

            actual = this.client.WhatShapeIsThis(0, 5, 4);
            Assert.AreEqual(expected, actual);

            actual = this.client.WhatShapeIsThis(1, 0, 4);
            Assert.AreEqual(expected, actual);

            actual = this.client.WhatShapeIsThis(2, 5, 0);
            Assert.AreEqual(expected, actual);

            actual = this.client.WhatShapeIsThis(1, 2, 3);
            Assert.AreEqual(expected, actual);

            actual = this.client.WhatShapeIsThis(10, 2, 3);
            Assert.AreEqual(expected, actual);
        }
    }
}

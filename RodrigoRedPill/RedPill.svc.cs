﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RodrigoRedPill
{
    [ServiceBehavior(Namespace = "http://KnockKnock.readify.net", IncludeExceptionDetailInFaults=true)]
    public class RedPill : IRedPill
    {
        public Guid WhatIsYourToken()
        {
            return new Guid("df946d67-936c-49fb-97b4-3e0ca256f941");
        }

        public long FibonacciNumber(long position)
        {
            // 64-bit integer limit for fibonacci is 92
            const int fibonacciLimit = 92;
            if (position > fibonacciLimit || position < (fibonacciLimit * -1))
            {
                // position passed is out of bounds
                string signal = (position > 0) ? ">" : "<";
                throw new ArgumentOutOfRangeException("position", "Fib(" + signal + fibonacciLimit + ") will cause a 64-bit integer overflow.");
            }

            long a = 0, b = 1, i = 0, desiredNumber = 0;
            // limit -> convert negative to positive
            long limit = (position > 0) ? position : (position * -1);

            while (i <= limit)
            {
                // 2 first numbers of sequence
                if (i == 0 || i == 1)
                {
                    desiredNumber = i;
                }
                else
                {
                    // adds the two past numbers of the sequence
                    desiredNumber = a + b;
                    a = b;
                    b = desiredNumber;
                }
                i++;
            }
            // check if the position wanted is negative and convert if necessary
            // only even positions are negative
            if (position < 0 && (position % 2) == 0)
            {
                desiredNumber *= -1;
            }
            return desiredNumber;
        }

        public string ReverseWords(string sentence)
        {
            if (sentence == null)
            {
                throw new ArgumentNullException("Value cannot be null");
            }

            // split the words separated by new line or space
            string[] words = sentence.Split(new string[] { "\r\n", "\n", " " }, StringSplitOptions.None);
            // for each word, reverse it and keep it in its index
            for (int idx = 0; idx < words.Length; idx++)
            {
                // convert the word to a char array
                char[] chars = words[idx].ToCharArray();
                // reverse the word
                Array.Reverse(chars);
                // put it back in the array on the same index
                words[idx] = new String(chars);
            }

            // didn't know how to join again with the same separators (\n, \r, etc).
            return String.Join(" ", words);
        }

        public TriangleType WhatShapeIsThis(int sideOne, int sideTwo, int sideThree)
        {
            // put the three sides on array so that can call the Distinct method
            // to determine the type of the triangle
            int[] sides = new int[3] 
            { 
                sideOne, sideTwo, sideThree
            };

            // check for basic conditions: all sides must be greater than zero
            // and the sum of any two sides have to be greater than the the third side
            if ((sideOne <= 0 || sideTwo <= 0 || sideThree <= 0) ||
                (sideOne + sideTwo <= sideThree) || (sideOne + sideThree <= sideTwo) ||
                (sideTwo + sideThree <= sideOne))
            {
                return TriangleType.Error;
            }

            if (sides.Distinct().Count() == 3) // Three distinct sides values
            {
                return TriangleType.Scalene;
            }

            if (sides.Distinct().Count() == 2) // Two distinct sides values
            {
                return TriangleType.Isosceles;
            }

            if (sides.Distinct().Count() == 1) // One distinct side -> all sides are equal
            {
                return TriangleType.Equilateral;
            }

            // None of above, return error
            return TriangleType.Error;
        }
    }
}

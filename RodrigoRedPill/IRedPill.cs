﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RodrigoRedPill
{
    [GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [ServiceContractAttribute(Namespace = "http://KnockKnock.readify.net")]
    public interface IRedPill
    {
        /// <summary>
        /// Returns my token that do the match between Readify system
        /// and my e-mail for recruitment proccess
        /// </summary>
        /// <returns>My token</returns>
        [OperationContract]
        Guid WhatIsYourToken();

        /// <summary>
        /// Returns the number of the position in Fibonacci sequence
        /// </summary>
        /// <param name="position">Position of number</param>
        /// <returns>The number corresponding the Fibonacci sequence number</returns>
        [OperationContract]
        [FaultContractAttribute(typeof(ArgumentOutOfRangeException), Name = "ArgumentOutOfRangeExceptionFault", Namespace = "http://schemas.datacontract.org/2004/07/System")]
        long FibonacciNumber(long n);

        /// <summary>
        /// Returns the word(s) passed as parameter reversed
        /// </summary>
        /// <param name="sentence">Sentence that will have its words reversed</param>
        /// <returns>All word(s) reversed (in the same order)</returns>
        [OperationContract]
        [FaultContractAttribute(typeof(ArgumentNullException), Name = "ArgumentNullExceptionFault", Namespace = "http://schemas.datacontract.org/2004/07/System")]
        string ReverseWords(string s);

        /// <summary>
        /// Return the type of the triangle based on the sides
        /// </summary>
        /// <param name="sideOne">side one</param>
        /// <param name="sideTwo">side two</param>
        /// <param name="sideThree">side three</param>
        /// <returns>Type of the triangle (Equilateral, Isosceles, Scalene)</returns>
        [OperationContract]
        TriangleType WhatShapeIsThis(int a, int b, int c);
    }

    /// <summary>
    /// Type used for the WhatShapeIsThis method
    /// </summary>
    [DataContract]
    public enum TriangleType
    {
        [EnumMember]
        Equilateral,
        [EnumMember]
        Error,
        [EnumMember]
        Isosceles,
        [EnumMember]
        Scalene
    }
}
